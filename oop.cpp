#include <iostream>
#include <ctime>
#include "oop.h"

using namespace std;


void Temperature(const CoffeeMachine &obj) {
    obj.Temperature();
}

void Price(const CoffeeMachine &obj) {
    obj.Price();
}

void CashDesk(const CoffeeMachine &obj, int money) {
    obj.CashDesk(money);
}
