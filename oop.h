#include <iostream>
#ifndef OOP_H
#define OOP_H

using namespace std;

class CoffeeMachine {
public:
    static int price;
    bool Start(int LevelWater) {
        if(CheckWater(LevelWater)) {
            //cout << "Please, wait about 1 minute, while drink will be ready." << endl;
            return true;
        }
        else {
            cout << "Sorry, low level of water! Add more water." << endl;
            return false;
        }
    }
    virtual void Temperature() const {
        cout << "Temperature of Your drink 100 degrees" << endl;
    }

    virtual void Price() const {
        cout << "Price of Your drink 10$" << endl;
    }

    virtual void CashDesk(int money) const {
        cout << "Sorry, the ticket office is temporarily closed. Take the money back: " << money << endl;
    }

    virtual void Sugar(int sugar) const {
        cout << "Add sugar spoons: " << sugar << endl;
    }


private:
    bool CheckWater(int LevelWater) {
        if(LevelWater > 100) {
            return true;
        }
        else {
            return false;
        }
    }
};

class Tea : public CoffeeMachine {
public:
    int price = 1;
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 80 degrees" << endl;
    }
    void Price() const {
        cout << "Price of Your drink " << price << "$" << endl;
    }
    void CashDesk(int money) const {
        if (price == money) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
        }
        if (price < money) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
            cout << "Take the change: " << money - price << endl;
        }
        if (price > money) {
            cout << "Sorry, not enough coins: " << price - money << endl;
        }

    }

};



class Coffee : public CoffeeMachine {
public:
    int price = 2;
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 70 degrees" << endl;
    }

    void Price() const {
        cout << "Price of Your drink " << price << "$" << endl;
    }

    void CashDesk(int money) const {
        if (price == money) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
        }
        if (price < money) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
            cout << "Take the change: " << money - price << endl;
        }
        if (price > money) {
            cout << "Sorry, not enough coins: " << price - money << endl;
        }

    }
};

class HotChocolate : public CoffeeMachine {
public:
    int price = 3;
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            return true;
        }
        else {
            cout << "Technical error" << endl;
            return false;
        }
    }
    void Temperature() const {
        cout << "Temperature of Your drink 60 degrees" << endl;
    }

    void Price() const {
        cout << "Price of Your drink " << price << "$" << endl;
    }

    void CashDesk(int money) const {
        if (price == money) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
        }
        if (price < money) {
            cout << "Please, wait about 1 minute, while drink will be ready." << endl;
            cout << "Take the change: " << money - price << endl;
        }
        if (price > money) {
            cout << "Sorry, not enough coins: " << price - money << endl;
        }

    }
};
void Temperature(const CoffeeMachine &obj);
void Price(const CoffeeMachine &obj);
void CashDesk(const CoffeeMachine &obj, int money);

#endif // OOP_H
