#include <iostream>
#include <ctime>
#include <oop.h>

using namespace std;



int main() {
    srand(time(NULL));
    int level = rand() % 1000;
    cout << "Level of water: " << level << endl;
    int option = 0;
    int money = 0;
    int sugar = 0;
    Tea t;
    Coffee c;
    HotChocolate h;
    cout << "Enter 1 - for coffee, 2 - for tea, 3 - hot chocolate: ";
    cin >> option;
    if(option == 1) {
        if(c.Start(level)) {
            Temperature(c);
            Price(c);
            cout << "How many spoonfuls of sugar? ";
            cin >> sugar;
            c.Sugar(sugar);
            cout << "Deposit money into the machine: ";
            cin >> money;
            CashDesk(c, money);
        }
    }
    else if(option == 2) {
        if(t.Start(level)) {
            Temperature(t);
            Price(t);
            cout << "How many spoonfuls of sugar? ";
            cin >> sugar;
            t.Sugar(sugar);
            cout << "Deposit money into the machine: ";
            cin >> money;
            CashDesk(t, money);
        }
    }
    else if(option == 3) {
        if(h.Start(level)) {
            Temperature(h);
            Price(h);
            cout << "How many spoonfuls of sugar? ";
            cin >> sugar;
            h.Sugar(sugar);
            cout << "Deposit money into the machine: ";
            cin >> money;
            CashDesk(h, money);
        }
    }
    return 0;
}
